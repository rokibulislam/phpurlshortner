<?php
class Database{

    private $host='localhost';
    private $username='root';
    private $password='';
    private $db='urlshortner';
    private $conn;

    public function __construct(){
        $this->con = mysqli_connect($this->host,$this->username,$this->password,$this->db);
        if ($this->con->connect_error) {
            die("Connection failed: " . $this->con->connect_error);
        }
    }

    public function shortCodeToUrl($url){
        if($this->ValidateUrl($url)==false){
            echo 'please enter valid url';
        }
    }

    public function ValidateUrl($url){
       return filter_var($url,FILTER_VALIDATE_URL,FILTER_FLAG_HOST_REQUIRED);
    }

    public function UrlExistInDb(){

    }

    public function  getShortUrl(){

        $result=array();
        $sql="select * from short_urls";
        echo $sql;
        $query=mysqli_query($this->con,$sql);
        while($row=mysqli_fetch_array($query,MYSQLI_ASSOC)){
            $result[]=$row;
        }
        return $result;
    }

    public function getShortUrlBylong_url($url){
        $url=mysqli_real_escape_string($this->con,$url);
        $result=array();
        $sql="select * from short_urls WHERE long_url='$url' LIMIT 1 ";
        echo $sql;
        $query=mysqli_query($this->con,$sql);
        while($row=mysqli_fetch_array($query,MYSQLI_ASSOC)){
            $result[]=$row;
        }
        return $result;
    }

    public function  insertShortUrl(){
        $sql="INSERT INTO short_urls (long_url, short_code, date_created, counter) VALUES ('Cardinal', 'Tom B. Erichsen', 'Skagen 21', 'Stavanger')";
        $query=mysqli_query($this->con,$sql);

    }

    public function deleteShortUrl(){
        $sql="DELETE FROM short_urls";
        mysqli_query($this->con,$sql);
    }

    public function close(){
        mysqli_close($this->conn);
    }


    public function nextLetter(&$str) {
            $str = ('z' == $str ? 'a' : ++$str);
    }

    public function getNextShortURL($s)
    {
        $a = str_split($s);
        $c = count($a);
        if (preg_match('/^z*$/', $s)) { // string consists entirely of `z`
            return str_repeat('a', $c + 1);
        }
        while ('z' == $a[--$c]) {
            $this->nextLetter($a[$c]);
        }
        $this->nextLetter($a[$c]);
        return implode($a);
    }
}